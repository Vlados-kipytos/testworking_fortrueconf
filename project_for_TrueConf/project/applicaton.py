"""
script to initialize api
"""
from flask import Flask, request
from config import add_json, read_json_id, PATH, update_json, read_json, delete_json
from http_status_codes import server_error, bad_request, not_found

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/api/v1/user/', methods=['GET'])
def show_user():
    """
    function for defining URL: /api/v1/user/
    """
    try:
        return read_json(PATH)
    except Exception:
        return server_error('Internal server error:')


@app.route('/api/v1/user/<id>', methods=['GET'])
def show_user_id(id):
    """
    function for defining URL: /api/v1/user/<id>
    """
    try:
        return read_json_id(PATH, id)
    except Exception:
        return server_error('Internal server error:')


@app.route('/api/v1/user/', methods=['POST'])
def add_user():
    """
    function for defining URL: /api/v1/user/
    """
    if not request.is_json or 'id' not in request.get_json() or 'name' not in request.get_json():
        return bad_request('Missing required data.')
    try:
        add_json(PATH, request.get_json())
        return 'Successful'
    except Exception:
        return server_error('Internal server error:')


@app.route('/api/v1/user_update/', methods=['PUT'])
def update_user_id():
    """
    function for defining URL: /api/v1/user_update/
    """
    if not request.is_json or 'id' not in request.get_json() or 'name' not in request.get_json():
        return bad_request('Missing required data.')
    try:
        update_json(PATH, request.get_json())
        return 'Successful'
    except Exception:
        return server_error('Internal server error:')


@app.route('/api/v1/delete/<id>', methods=['GET'])
def delete_user_id(id):
    """
    function for defining URL: /api/v1/delete/<id>
    """
    try:
        delete_json(PATH, id)
        return 'Successful'
    except Exception:
        return server_error('Internal server error:')


if __name__ == '__main__':
    app.run()
