"""
script for to initialize operations
"""
from http_status_codes import server_error, bad_request, not_found
import json
import os

DIRECTORY = '/home'
PATH = os.path.join(DIRECTORY, "vladialv", "test_directory", "example.txt")


def read_json(path):
    """
    function for read data
    """
    with open(path, 'r+') as file_in:
        data = file_in.read()
        return data


def read_json_id(path, id):
    """
    function for read data through unique id
    """
    with open(path, 'r+') as file_in:
        data = eval(file_in.read())
        for val in data:
            if val['id'] == id:
                return val
        return server_error('User does not exists')


def add_json(path, json_data, List=[]):
    """
    function for add data
    """
    value = True
    with open(path, 'r+') as file_out:
        if os.path.getsize(path) > 0:
            data = eval(file_out.read())
            for val in data:
                if val['id'] == json_data['id']:
                    value = False
            if value:
                data.append(json_data)
                file_out.seek(0)
                file_out.truncate()
                json.dump(data, file_out)
        else:
            List.append(json_data)
            json.dump(List, file_out)


def update_json(path, json_data):
    """
    function for update data
    """
    with open(path, 'r+') as file_out:
        if os.path.getsize(path) > 0:
            data = eval(file_out.read())
            for num, val in enumerate(data):
                if val['id'] == json_data['id']:
                    data[num] = json_data
                    file_out.seek(0)
                    file_out.truncate()
                    json.dump(data, file_out)
        else:
            return server_error('User does not exists')


def delete_json(path, id):
    """
    function for delete data
    """
    with open(path, 'r+') as file_in:
        if os.path.getsize(path) > 0:
            data = eval(file_in.read())
            for num, val in enumerate(data):
                if val['id'] == id:
                    data.remove(val)
                    file_in.seek(0)
                    file_in.truncate()
                    json.dump(data, file_in)
        else:
            return server_error('User does not exists')
