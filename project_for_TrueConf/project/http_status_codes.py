"""
script for initialize HHTP_status_sode
"""
from flask import jsonify


def server_error(message):
    """
    function for error fixing with status code = 500
    """
    response = jsonify({'error': message})
    response.status_code = 500
    return response


def bad_request(message):
    """
    function for error fixing with status code = 400
    """
    response = jsonify({'error': message})
    response.status_code = 400
    return response


def not_found(message):
    """
    function for error fixing with status code = 404
    """
    response = jsonify({'error': message})
    response.status_code = 404
    return response
